import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface TaskState {
  tasks: any[];
}

const initialState: TaskState = {
  tasks: [],
};

function generarID(length: number) {
    let caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let id = '';
    for (let i = 0; i < length; i++) {
      let indice = Math.floor(Math.random() * caracteres.length);
      id += caracteres.charAt(indice);
    }
    return id;
}

  
const tasksSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    addTask: (state, action: PayloadAction<string>) => {

      state.tasks.push({task: action.payload, state: 0, id: generarID(8)});
    },
    updateTask: (state, action: PayloadAction<number>) => {
        let index = state.tasks.findIndex(item => item.id === action.payload);
        state.tasks[index]['state'] = 1;
    },
    removeTask: (state, action: PayloadAction<number>) => {
        state.tasks = state.tasks.filter(item => item.id !== action.payload);
    },
  },
});

export const { addTask, removeTask, updateTask } = tasksSlice.actions;
export default tasksSlice.reducer;
