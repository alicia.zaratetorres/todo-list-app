import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Tasks } from './complements/Tasks';
import { HeaderBar } from './complements/HeaderBar';

function App() {
  return (
    <>
      <h1>TODO App Alicia</h1>
      <div className="app-container">
        <br />
        <HeaderBar/>
        <Tasks/>
      </div>
    </>
  );
}

export default App;
