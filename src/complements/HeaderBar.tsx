import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Button from 'react-bootstrap/Button';
import { useDispatch } from 'react-redux';
import { addTask } from '../tasksSlice';

export const HeaderBar = () => {
  const [inputValue, setInputValue] = useState('');
  const dispatch = useDispatch();

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  };

  const createTask = () => {
    if (inputValue.trim()) {
      dispatch(addTask(inputValue));
      setInputValue('');
    }
  };

  const handleKeyPress = (event: React.KeyboardEvent) => {
    if (event.key === 'Enter') {
      createTask();
    }
  };

  return (
    <div>
      <InputGroup className="mb-3">
        <Form.Control
          placeholder="Describe la tarea"
          aria-label="Describe la tarea"
          aria-describedby="basic-addon2"
          value={inputValue}
          onChange={handleInputChange}
          onKeyUp={handleKeyPress}
        />
        <Button variant="outline-primary" id="button-addon2" onClick={createTask}>
          Agregar tarea
        </Button>
      </InputGroup>
    </div>
  );
};
