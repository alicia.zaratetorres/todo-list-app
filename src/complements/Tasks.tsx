import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import { useDispatch } from 'react-redux';
import { removeTask, updateTask } from '../tasksSlice';
import './Tasks.css';

export const Tasks = () => {
    const tasks = useSelector((state: RootState) => state.tasks.tasks);
    const dispatch = useDispatch();

    const checkTask = (index: number) => {
        dispatch(updateTask(index));
    }
    const deleteTask = (index: number) => {
        dispatch(removeTask(index));
    };

    // Filtrar las tareas basadas en el valor de state
    const tasksWithState0 = tasks.filter(task => task.state === 0);
    const tasksWithState1 = tasks.filter(task => task.state === 1);

    return (
        <div className="tasks-container">
            <div style={{ maxHeight: '400px', overflowY: 'auto' }}>
                <ListGroup>
                    {tasksWithState0.map((item, index) => (
                        <ListGroup.Item key={index}>
                            <Container>
                                <Row>
                                    <Col className='task' xs={10}>
                                        <Form.Check
                                            id={`default-checkbox_${item.id}`}
                                            label={item.task}
                                            checked={item.state === 1}
                                            onChange={() => checkTask(item.id)}/>
                                    </Col>
                                    <Col xs={2}>
                                        <button id='btn-trash' className='btn btn-outline-danger' onClick={() => deleteTask(item.id)}>
                                        Borrar
                                        </button>
                                    </Col>
                                </Row>
                            </Container>
                        </ListGroup.Item>
                    ))}
                </ListGroup>
            </div>

            <br />

            <div>
                <Accordion defaultActiveKey="1">
                    <Accordion.Item eventKey="0">
                        <Accordion.Header>Terminadas</Accordion.Header>
                        <Accordion.Body>
                            <div style={{ maxHeight: '300px', overflowY: 'auto' }}>
                                <ListGroup>
                                    {tasksWithState1.map((item, index) => (
                                        <ListGroup.Item key={index}>
                                        <Container>
                                            <Row>
                                                <Col className='task-terminated' xs={11}>{item.task}</Col>
                                            </Row>
                                        </Container>
                                        </ListGroup.Item>
                                    ))}
                                </ListGroup>
                            </div>
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            </div>
        </div>
    );
};